addSbtPlugin("org.scala-js" % "sbt-scalajs" % "0.6.14")

addSbtPlugin("com.lihaoyi" % "workbench" % "0.3.0")

addCompilerPlugin("org.scalamacros" % "paradise" % "2.1.0" cross CrossVersion.full)

addSbtPlugin("com.geirsson" % "sbt-scalafmt" % "0.5.4")
